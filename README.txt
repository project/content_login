--------------
Content Login
--------------

-----------
DESCRIPTION
-----------
Normally a user gets login by login box containing user name and user password.
Now this module tries to login an user by providing content url which is
encrypted.
This module is useful when you want to provide demonstration of site to new user
and you do not want them to perform provide user name and password and then
enter into the website for walk thorugh of the site.

By encrypted url of the content user will be able to go to the content and he
will be able to logged into the application without any authentication.

This module accepts content id or node id and generates a encrypted URL which
will logged in test user and direct to content page.

-----------------------
Drupal required version
-----------------------
Drupal 7.x

----------
INSTALLING
----------
1. Copy the 'content_login' folder to your sites/all/modules directory.

2. Go to admin/modules and enable the module.
Refer instalation modules at http://drupal.org/node/70151

-------------
CONFIGURATION
-------------
Go to admin/structure/contentlogin/settings. Follow instructions on this page.
